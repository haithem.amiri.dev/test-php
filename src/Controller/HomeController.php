<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Service\Helper;
use App\Entity\Carte;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
    	return $this->render('home/index.html.twig', [
    		'controller_name' => 'HomeController',
    	]);
    }

 	/**
     * @Route("/get-cards", name="ajax_get_cards", methods={"POST"})
     */
 	public function getCards(SerializerInterface $serializer, Helper $helper): Response
 	{
 		$cartes = [];

 		for ($i=0; $i < 10; $i++) { 
 			$carte = new Carte();

 			$valeur = $helper->getRandomValue();
 			$couleur = $helper->getRandomColor();

 			$carte->setValeur($valeur['value']);
 			$carte->setLibeleValeur($valeur['label']);
 			$carte->setCouleur($couleur['value']);
 			$carte->setLibeleCouleur($couleur['label']);
 			$cartes[] = $carte; 	
 		}

 		return new JsonResponse($serializer->normalize($cartes));   
 	}

 	/**
     * @Route("/sort-cards", name="ajax_sort_cards", methods={"POST"})
     */
 	public function sortCards(Request $request, SerializerInterface $serializer, Helper $helper): Response
 	{
 		$dataTab = $request->request->all();    
 		$cartes = [];
 
 		for ($i=0; $i < count($dataTab['cardValues']) -1 ; $i++) { 

 			for ($j=$i+1; $j < count($dataTab['cardValues']) ; $j++) { 

 				if ($dataTab['cardValues'][$i][1] > $dataTab['cardValues'][$j][1]){
 					$tmp = $dataTab['cardValues'][$j];
 					$dataTab['cardValues'][$j] = $dataTab['cardValues'][$i];
 					$dataTab['cardValues'][$i] = $tmp;
 				}
 				else if ($dataTab['cardValues'][$i][1] == $dataTab['cardValues'][$j][1]){
 					if ($dataTab['cardValues'][$i][0] > $dataTab['cardValues'][$j][0]){
 						$tmp = $dataTab['cardValues'][$j];
 						$dataTab['cardValues'][$j] = $dataTab['cardValues'][$i];
 						$dataTab['cardValues'][$i] = $tmp;
 					}
 				}
 			}
 		}

 		for ($i=0; $i < count($dataTab['cardValues']) ; $i++) { 
 
 			$carte = new Carte();

 			$valeur = $helper->getRandomValue();
 			$couleur = $helper->getRandomColor();

 			$carte->setValeur($dataTab['cardValues'][$i][0]);
 			$carte->setLibeleValeur(Carte::LISTE_DES_VALEURS[$dataTab['cardValues'][$i][0]]);
 			$carte->setCouleur($dataTab['cardValues'][$i][1]);
 			$carte->setLibeleCouleur(Carte::LISTE_DES_COULEURS[$dataTab['cardValues'][$i][1]]);
 			$cartes[] = $carte; 	
 		}

 		return new JsonResponse($serializer->normalize($cartes));   
 	}


 }
