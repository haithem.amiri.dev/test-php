<?php

namespace App\Entity;

use App\Repository\CarteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarteRepository::class)
 */
class Carte
{
    public const LISTE_DES_COULEURS = ["Carreaux", "Coeur", "Pique", "Trèfle"];
    public const LISTE_DES_VALEURS = ["As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi"];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $valeur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libeleValeur;

    /**
     * @ORM\Column(type="integer")
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libeleCouleur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?int
    {
        return $this->valeur;
    }

    public function setValeur(int $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getLibeleValeur(): ?string
    {
        return $this->libeleValeur;
    }

    public function setLibeleValeur(string $libeleValeur): self
    {
        $this->libeleValeur = $libeleValeur;

        return $this;
    }

    public function getCouleur(): ?int
    {
        return $this->couleur;
    }

    public function setCouleur(int $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getLibeleCouleur(): ?string
    {
        return $this->libeleCouleur;
    }

    public function setLibeleCouleur(string $libeleCouleur): self
    {
        $this->libeleCouleur = $libeleCouleur;

        return $this;
    }
}
