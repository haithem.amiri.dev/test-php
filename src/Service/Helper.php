<?php
namespace App\Service;

use App\Entity\Carte;

class Helper
{
	public function getRandomColor(): Array
	{
		$color["value"] = rand(0, 3);
		$color["label"] = Carte::LISTE_DES_COULEURS[$color["value"]];
		return $color;
	}

	public function getRandomValue(): Array
	{
		$valeur["value"] = rand(0, 12);
		$valeur["label"] = Carte::LISTE_DES_VALEURS[$valeur["value"]];
		return $valeur;
	}
}